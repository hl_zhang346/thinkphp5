$(function()
{
    $('#DataGrid').datagrid({
        width:600,
        title:'list of city',
        iconCls:'icon-search',
        url:'GetListCity',
        pagination:true,
        pageSize:10,       
  
        columns:[[
            {
                title:'city_id',
                field:'city_id',

            },
            {
                title: 'city',
                field: 'city',

            },
            {
                title: 'country_id',
                field: 'country_id',
                width:80,
            },
            {
                title: 'last_update',
                field: 'last_update',
                width:200,
            },
        ]],
    });
});
