<?php

    namespace app\index\model; 
     
    use think\Model;  

    class Index extends Model
    {  
        protected $table = 'city';
        protected $pk = 'city_id'; 
        protected $auto = ['city_id']; 
     
    }  
