<?php
namespace app\index\controller;

use think\Controller;
use think\Request;
use think\Db;
use think\Route;


class Index extends Controller
{
    public function index()
    {
        return $this->fetch('hello.html');;      
    }
    

    public function hello()
    {
        // 查询数据             
        return $this->fetch();      
    }

      public function GetListCity()
    {
        // 查询数据      
        //$result  = IndexModel::All();  
        $City = new \app\index\model\Index();


        $page = $_POST['page'];
        $rows = $_POST['rows'];

        $result=$City->paginate($rows);
        //$result=$City::all();
        return json($result);     
    }

    public function myPhp()
    {
        return phpinfo();
    }


    public function test()
    {
        $str = "123.409838393abc";
        settype($str, "double");
        echo "int =". $str;
    }


    public function webapi()
    {
     // 查询数据
        $result = Db::table('city')
                    ->page(1, 100)
                    ->select();

        return json($result);
    }
}
